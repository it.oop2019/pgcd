[![pipeline status](https://gitlab.com/kloba/pgcd/badges/master/pipeline.svg)](https://gitlab.com/kloba/pgcd/pipelines)

## Quick-start:

### 1. Create a fork of this project
![Make fork](/img/make_fork.png)

### 2. Add your tests
![Change test](/img/change_tests.png)

### 3. Change connections to your databases in variable
![Change variables](/img/change_variables.png)

### 4. Profit! Start using Continuous Database Delivery
![Pipeline](/img/pipeline.png)
